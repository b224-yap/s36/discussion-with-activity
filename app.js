//  Setup imports

const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");

// This allows us to use all the routes defined in the "taskRoutes"
const taskRoutes = require("./routes/taskRoutes.js");


// Express Setup
const app = express();
const port = 3001;


// Initialize dotenv
dotenv.config();

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended : true}))

// Mongoose Setup

mongoose.connect(`mongodb+srv://yap_224:${process.env.MONGODB_PW}@224-yap.ccjh9ka.mongodb.net/s36?retryWrites=true&w=majority`, 
		{
			useNewUrlParser: true,
			useUnifiedTopology : true
		}

	);

let db = mongoose.connection
db.on('error', () =>console.error('Connection Error'))
db.once('open', () => console.log('Connection to MongoDb!'));

// Mail URI
// http ://localhost:3001/tasks/<endpoint>
app.use("/tasks", taskRoutes);


app.listen(port,() => console.log(`Server is running at port: ${port}`));